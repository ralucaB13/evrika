package tasks.integration;

import org.junit.jupiter.api.*;
import org.mockito.MockitoAnnotations;
import tasks.model.Task;
import tasks.repository.LinkedTaskRepository;
import tasks.services.TaskService;
import tasks.stub.TaskStub;
import tasks.validators.ValidatorException;

public class IntegrationRepositoryServiceStub {
    private LinkedTaskRepository repo;
    private TaskService service;

    @BeforeAll
    static void setup() {
        System.out.println("Testing started");
    }

    @AfterAll
    static void tearDown() {
        System.out.println("Testing stopped");
    }

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(IntegrationRepositoryServiceStub.class);
        repo = new LinkedTaskRepository();
        service = new TaskService();
        service.setLinkedTaskRepository(repo);
    }

    @AfterEach
    void tearDownEach() {
        repo = null;
        service = null;
    }

    @Test
    public void addValidTask() {
        TaskStub taskStub = TaskStub.getStub();
        Assertions.assertEquals(service.getAll().size(), 0);
        repo.add(taskStub);
        Assertions.assertEquals(service.getAll().size(), 1);
    }

    @Test
    public void addInvalidTask() {
        TaskStub taskStubInvalid = TaskStub.getStubInvalid();
        Assertions.assertEquals(service.getAll().size(), 0);
        Assertions.assertThrows(ValidatorException.class, () -> repo.add(taskStubInvalid));
        Assertions.assertEquals(service.getAll().size(), 0);
    }


}
