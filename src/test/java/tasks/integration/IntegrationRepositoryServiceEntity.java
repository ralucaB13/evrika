package tasks.integration;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import tasks.model.Task;
import tasks.repository.LinkedTaskRepository;
import tasks.services.TaskService;
import tasks.validators.ValidatorException;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class IntegrationRepositoryServiceEntity {
    private Task task;
    private LinkedTaskRepository repo;
    private TaskService service;

    @BeforeAll
    static void setup() {
        System.out.println("Testing started");
    }

    @AfterAll
    static void tearDown() {
        System.out.println("Testing stopped");
    }

    @BeforeEach
    void setUp(){
        String title = "Task";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        task = new Task(title, start);
        repo = new LinkedTaskRepository();
        service = new TaskService();
        service.setLinkedTaskRepository(repo);
    }

    @AfterEach
    void tearDownEach()  {
        repo = null;
        service = null;
        task = null;
    }

    @Test
    public void addTaskValid() {
        Assertions.assertEquals(service.getAll().size(), 0);
        repo.add(task);
        Assertions.assertEquals(service.getAll().size(), 1);
    }

    @Test
    public void addTaskInvalid() {
        task.setTitle("");
        Assertions.assertEquals(service.getAll().size(), 0);
        Assertions.assertThrows(ValidatorException.class, () -> repo.add(task));
        Assertions.assertEquals(service.getAll().size(), 0);
    }
}
