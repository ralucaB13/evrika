package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import tasks.model.Task;
import tasks.repository.LinkedTaskRepository;
import tasks.validators.ValidatorException;

import java.io.File;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TaskServiceTest {

    @Mock
    private LinkedTaskRepository linkedTaskRepository;

    @Spy
    @InjectMocks
    private TaskService taskService = new TaskService();


    @Test
    public void testTaskServiceAdd() throws Exception {

        String title = "Task";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);
        ObservableList<Task> taskList = FXCollections.observableArrayList(Collections.singletonList(task));
        Mockito.verify(linkedTaskRepository, Mockito.times(0)).add(task);
        TaskService.setSavedTasksFile(new File("classpath:tasks.txt"));
        taskService.rewriteFile(taskList);
        Mockito.verify(linkedTaskRepository, Mockito.times(1)).add(task);

    }


    @Test
    public void testTaskServiceAddInvalid() throws Exception {

        String title = "";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);
        ObservableList<Task> taskList = FXCollections.observableArrayList(Collections.singletonList(task));
        Mockito.verify(linkedTaskRepository, Mockito.times(0)).add(task);
        TaskService.setSavedTasksFile(new File("classpath:tasks.txt"));

        try {
            taskService.rewriteFile(taskList);
        } catch (ValidatorException ex) {
            Assertions.assertEquals("Descriere invalida!\n", ex.getMessage());
        }

        Mockito.verify(linkedTaskRepository, Mockito.times(0)).add(task);

    }
}

