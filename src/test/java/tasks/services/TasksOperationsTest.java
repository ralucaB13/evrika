package tasks.services;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.model.Task;
import tasks.validators.ValidatorException;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TasksOperationsTest {

    private TasksOperations tasksOperations;
    private TasksOperations nullTasksOperations;
    private ArrayList<Task> incomingTasks;
    private Date start, end;

    @BeforeEach
    void setUp() {

        incomingTasks = new ArrayList<>();
        start = new GregorianCalendar(2020, Calendar.MARCH, 10, 10, 0, 0).getTime();
        end = new GregorianCalendar(2020, Calendar.APRIL, 14, 10, 0, 0).getTime();

        Date d1 = new GregorianCalendar(2020, Calendar.MARCH, 23, 10, 0, 0).getTime();
        Date d2 = new GregorianCalendar(2020, Calendar.APRIL, 11, 12, 0, 0).getTime();
        Date d3 = new GregorianCalendar(2020, Calendar.APRIL, 10, 12, 0, 0).getTime();
        Task task1 = new Task("task1", d1, d2, 60);
        task1.setActive(true);

        Task task2 = new Task("task2", d1, d3, 120);
        task2.setActive(true);

        Task task3 = new Task("task3", d2, d3, 180);
        task3.setActive(true);

        incomingTasks.addAll(Arrays.asList(task1, task2, task3));
        tasksOperations = new TasksOperations(FXCollections.observableArrayList(incomingTasks));
        nullTasksOperations = new TasksOperations(FXCollections.observableArrayList(new ArrayList<>()));
    }

    @AfterEach
    void tearDown() {
    }

    /*
        Perioada de timp valida
        start = 10/03/2020 10:00
        end = 14/04/2020 10:00
     */
    @Test
    public void F02_TC01_valid() {

        List<Task> result = new ArrayList<>();
        tasksOperations.incoming(start, end).forEach(result::add);
        assertEquals(3, result.size());
    }

    /*
        Perioada de timp non valida
        start = 10/03/2020 10:00
        end = null
     */
    @Test
    public void F02_TC01_nonValid() {

        assertThrows(ValidatorException.class, () -> tasksOperations.incoming(start, null));
    }

    @Test
    public void F02_TC02_valid() {

        List<Task> result = new ArrayList<>();
        nullTasksOperations.incoming(start, end).forEach(result::add);
        assertEquals(0, result.size());
    }


    @Test
    public void F02_TC02_nonValid() {

        assertThrows(ValidatorException.class, () -> tasksOperations.incoming(null, end));
    }
}
