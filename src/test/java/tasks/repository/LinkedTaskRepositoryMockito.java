package tasks.repository;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import tasks.model.Task;
import tasks.validators.ValidatorException;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@MockitoSettings(strictness = Strictness.LENIENT)
public class LinkedTaskRepositoryMockito {

    @Mock
    private LinkedTaskRepository linkedTaskRepository;

    @Test
    public void testAddTaskRepository() {

        String title = "Task";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);
        Mockito.doNothing().when(linkedTaskRepository).add(task);
        Mockito.when(linkedTaskRepository.size()).thenReturn(1);
        Assertions.assertEquals(linkedTaskRepository.size(), 1);
    }

    @Test
    public void testAddTaskRepositoryThrowsException() {

        String title = "T" + RandomStringUtils.randomAlphanumeric(251) + "1234";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);
        Mockito.doThrow(ValidatorException.class).when(linkedTaskRepository).add(task);
        Assertions.assertEquals(linkedTaskRepository.size(), 0);
    }
}
