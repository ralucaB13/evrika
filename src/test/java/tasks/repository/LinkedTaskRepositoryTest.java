package tasks.repository;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.*;
import tasks.model.Task;
import tasks.validators.ValidatorException;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Tests for LinkedTaskRepositoryTest class")
class LinkedTaskRepositoryTest {

    private LinkedTaskRepository repo;

    @BeforeEach
    void setUp() {

        repo = new LinkedTaskRepository();
    }

    @Test
    @Tag("addTask")
    public void tc01_ECP_valid() {//valid descriere

        String title = "Task";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);

        repo.add(task);
        assertEquals(1, repo.size());
    }

    @Test
    @Tag("addTask")
    public void tc03_BVA_valid() {//valid descriere: len 255

        String title = "T" + RandomStringUtils.randomAlphanumeric(251) + "123";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);

        repo.add(task);
        assertEquals(1, repo.size());
    }

    @Test
    @Tag("addTask")
    public void tc02_BVA_valid() { //valid pentru descriere : len 1

        String title = "T";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);

        repo.add(task);
        assertEquals(1, repo.size());
    }

    @Test
    @Tag("addTask")
    public void tc04_BVA_valid() { //valid pentru descriere: len 2

        String title = "Tk";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);

        repo.add(task);
        assertEquals(1, repo.size());
    }

    @Test
    @Tag("addTask")
    public void tc05_BVA_valid() { //valid pentru descriere: len 254

        String title = "T" + RandomStringUtils.randomAlphanumeric(251) + "12";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);

        repo.add(task);
        assertEquals(1, repo.size());
    }


    @Test
    @Tag("addTask")
    public void tc06_BVA_nonValid() { //non-valid pentru descriere: len 0

        String title = "";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);
        task.setActive(true);

        assertThrows(ValidatorException.class, () -> repo.add(task));
        assertEquals(0, repo.size());
    }

    @Test
    @Tag("addTask")
    public void tc07_BVA_nonValid() { //non-valid pentru descriere: len 256

        String title = "T" + RandomStringUtils.randomAlphanumeric(251) + "1234";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);

        assertThrows(ValidatorException.class, () -> repo.add(task));
        assertEquals(0, repo.size());
    }

    ////////////////////////////////////////DATA////////////////////////////////////////////////////////////////////////////

    @Test
    @Tag("addTask")
    public void tc08_ECP_nonValid() {//non-valid for date - 25/07/1970

        String title = "Task";
        Date start = new GregorianCalendar(1970, Calendar.JULY, 25).getTime();
        Task task = new Task(title, start);

        assertThrows(ValidatorException.class, () -> repo.add(task));
        assertEquals(0, repo.size());
    }

    @Test
    @Tag("addTask")
    @Timeout(5)
    public void tc09_BVA_valid() {//valid pentru data - capatul inferior al intervalului 01/01/1971

        String title = "Task";
        Date start = new GregorianCalendar(1971, Calendar.JANUARY, 1).getTime();
        Task task = new Task(title, start);

        repo.add(task);
        assertEquals(1, repo.size());
    }

    @Test
    @Tag("addTask")
    public void tc10_BVA_valid() {//valid pentru data - capatul inferior al intervalului+1 02/01/1971

        String title = "Task";
        Date start = new GregorianCalendar(1971, Calendar.JANUARY, 2).getTime();
        Task task = new Task(title, start);

        repo.add(task);
        assertEquals(1, repo.size());
    }

    @Test
    @Tag("addTask")
    public void tc11_BVA_nonValid() {//non-valid pentru data - capatul inferior al intervalului-1 31/12/1970

        String title = "Task";
        Date start = new GregorianCalendar(1970, Calendar.DECEMBER, 31).getTime();
        Task task = new Task(title, start);

        assertThrows(ValidatorException.class, () -> repo.add(task));
        assertEquals(0, repo.size());
    }
}
