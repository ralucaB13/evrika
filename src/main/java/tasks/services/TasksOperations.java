package tasks.services;

import javafx.collections.ObservableList;
import org.apache.log4j.Logger;
import tasks.model.Task;
import tasks.validators.ValidatorException;

import java.util.*;


public class TasksOperations {

    public ArrayList<Task> tasks;
    private Logger logger = Logger.getLogger(TasksOperations.class);

    public TasksOperations(ObservableList<Task> tasksList){
        tasks=new ArrayList<>();
        tasks.addAll(tasksList);
    }

    public Iterable<Task> incoming(Date start, Date end){

        logger.info(start);
        logger.info(end);
         ArrayList<Task> incomingTasks = new ArrayList<>();
         if(start == null || end == null){
            throw new ValidatorException("Start or end date cannot be null");
        }

         if(tasks != null) {
             for (Task t : tasks) {
                Date nextTime = t.nextTimeAfter(start);
                if (nextTime != null && (nextTime.before(end) || nextTime.equals(end))) {
                    incomingTasks.add(t);
                    logger.info(t.getTitle());
                }
                  else{ throw new ValidatorException("invalid task!"); }
            }

             return incomingTasks;
        }
         else{ return null; }

    }
    public SortedMap<Date, Set<Task>> calendar(Date start, Date end){
        Iterable<Task> incomingTasks = incoming(start, end);
        TreeMap<Date, Set<Task>> calendar = new TreeMap<>();

        for (Task t : incomingTasks){
            Date nextTimeAfter = t.nextTimeAfter(start);
            while (nextTimeAfter!= null && (nextTimeAfter.before(end) || nextTimeAfter.equals(end))){
                if (calendar.containsKey(nextTimeAfter)){
                    calendar.get(nextTimeAfter).add(t);
                }
                else {
                    HashSet<Task> oneDateTasks = new HashSet<>();
                    oneDateTasks.add(t);
                    calendar.put(nextTimeAfter,oneDateTasks);
                }
                nextTimeAfter = t.nextTimeAfter(nextTimeAfter);
            }
        }
        return calendar;
    }
}
