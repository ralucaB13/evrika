package tasks.validators;

import tasks.model.Task;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class TaskValidator {

    public static void validateTask(Task task) {

        String err = "";
        if (task.getTitle().length() < 1 || task.getTitle().length() > 255) {
            err += "Descriere invalida!\n";
        }
        if (task.getStartTime().before(new GregorianCalendar(1971,  Calendar.JANUARY,1).getTime())) {
            err += "Data invalida!\n";
        }
        if (!err.equals("")) {
            throw new ValidatorException(err);
        }
    }
}
