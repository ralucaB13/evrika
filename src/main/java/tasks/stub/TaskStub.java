package tasks.stub;

import tasks.model.Task;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TaskStub extends Task {

    public TaskStub(String title, Date time) {
        super(title, time);
    }

    public static TaskStub getStub() {
        String title = "Task";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        return new TaskStub(title, start);
    }

    public static TaskStub getStubInvalid() {
        String title = "";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        return new TaskStub(title, start);
    }

    public String getTitle() {
        return super.getTitle();
    }

    public Date getDate() {
        return super.getStartTime();
    }
}
